<?php

        header('Access-Control-Allow-Origin: *');
        header('content-Type: application/json');

        include_once '../../config/data.php';
        include_once '../../models/post.php';

        $database = new Data();
        $db = $database->connect();

        $post = new Post($db);
        $post->id=isset($_GET['departmentid']) ? $_GET['departmentid'] : die();
        $post->read_single();
        $post_arr = array(
            'departmentid' => $post->departmentid,
            'departmentname' => $post->departmentname,
            'description' => $post->description,
            'status' => $post->status
        );
        print_r(json_encode($post_arr));
?>