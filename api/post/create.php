<?php

        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers:Access-Control-Allow-Headers,content-Type,Access-Control-Allow-Methods,Authorization,X-requested-With');

        include_once '../../config/data.php';
        include_once '../../models/post.php';

        $database = new Data();
        $db = $database->connect();

        $post = new Post($db);
        $data = json_decode(file_get_contents("php://input"));

        $post->departmentname = $data->departmentname;
        $post->description = $data->description;
        $post->status = $data->status;

        if ($post->create()) {
            echo json_encode(
                array('Message' => 'Post create'));
        }
        else{
            echo json_encode(
                array('Message' => 'Post not create'));
        }



?>