<?php
include "config.php";
$messageJson= new StdClass();
$messageJson->data=null;
try {  
$pdo=new PDO(URI_BASE_DONNEES, UTILISATEUR, MOT_DE_PASSE);
$stm = $pdo->query("SELECT * FROM department");

if ($stm)
{
$contacts= $stm->fetchAll();
$messageJson->data=$contacts;
}
else
{
$messageJson->status=400;
$messageJson->error="Erreur dans la requête SQL";
}

}
catch (Exception $e)
{
$messageJson->status=300;
$messageJson->error=$e->getMessage();
}
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
echo json_encode($messageJson);  

?>