<?php 

    class Post{
        private $conn;
        private $table='department';

        public $departmentid;
        public $departmentname;
        public $description;
        public $status;
        

        public function __construct($db)
        {
            $this->conn=$db;
        }

        public function read(){
            $query = 'SELECT * FROM '. $this->table;

            $stmt=$this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        public function read_single(){

        $query = 'SELECT * FROM ' . $this->table .' WHERE departmentid=? LIMIT 0,1';
        $stmt=$this->conn->prepare($query);
        $stmt->bindParam(1,$this->departmentid);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->departmentid = $row['departmentid'];
        $this->departmentname = $row['departmentname'];
        $this->description=$row['description'];
        $this->status=$row['status'];

        }

        public function create(){
            $query = 'INSERT INTO '. $this->table . ' SET departmentname= :departmentname,description = :description,status= :status';
            $stmt=$this->conn->prepare($query);
            $this->departmentname = htmlspecialchars(strip_tags($this->departmentname));
            $this->description = htmlspecialchars(strip_tags($this->description));
            $this->status = htmlspecialchars(strip_tags($this->status));
            $stmt->bindParam(':departmentname',$this->departmentname);
            $stmt->bindParam(':description',$this->description);
            $stmt->bindParam(':status',$this->status);
           if ( $stmt->execute()) {
               return true;
           }

        }

    }

?>